/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

describe("PADIDiveTable", function() { 
	var table;
	var pressureGroup;
	var adjMaxDiveTime;
	beforeEach(function(done){
        table = new PADIDiveTable(); 
        table.initDiveTable(function () { done(); } );
    });
	
	// Table 1 ******************************************************************
	describe("when given a dive of 60 feet for 21 minutes", function(){ 
		it("should give a pressure group of G", function() { 
			pressureGroup = table.getEndOfDiveGroup(60, 21); 
			expect(pressureGroup).toBe('G'); //
		});
	});
    
	describe("when given a dive of 140 feet for 15 minutes", function(){
		it("should give a value indicating an unsupported dive depth (undefined)", function() {
			pressureGroup = table.getEndOfDiveGroup(140, 15);            
			expect(pressureGroup).toBe(undefined);
		});
	});
	
	describe("when given a dive of 0 feet for 15 minutes", function(){
		it("should give a pressure group of B", function() {
			pressureGroup = table.getEndOfDiveGroup(0, 15);            
			expect(pressureGroup).toBe('B');
		});
	});
    
	describe("when given a dive of 80 feet for 0 minutes", function(){
		it("should give a pressure group of A", function() {
			pressureGroup = table.getEndOfDiveGroup(80, 0);            
			expect(pressureGroup).toBe('A');
		});
	});
	
	// Table 2 ******************************************************************
	describe("when given an initial pressure group of H and a SIT of 25 minutes", function(){
		it("should give a pressure group of E", function() {
			pressureGroup = table.getSurfaceIntervalTimeGroup('H', 25);
			expect(pressureGroup).toBe('E');
		});
	});
	
	describe("when given an initial pressure group of A and a SIT of 10000 minutes", function(){
		it("should give a pressure group of undefined indicating that it has been more than 24 hours", function() {
			pressureGroup = table.getSurfaceIntervalTimeGroup('A', 10000);
			expect(pressureGroup).toBe(undefined);
		});
	});
    
	describe("when given an initial pressure group of F and a SIT of 0 minutes", function(){
		it("should give a pressure group of F", function() {
			pressureGroup = table.getSurfaceIntervalTimeGroup('F', 0);
			expect(pressureGroup).toBe('F');
		});
	});
	
	//Table 3 ******************************************************************
	describe("when given an repetitive dive pressure group of D and a depth of 60 feet", function(){
		it("should give an actual bottom time of 39", function() {
			adjMaxDiveTime = table.getRepetitiveDiveMaxTime('D', 60);
			expect(adjMaxDiveTime).toBe(39);
		});
	});
    
	describe("when given an repetitive dive pressure group of A and a depth of 0 feet", function(){
		it("should give an actual bottom time of 195", function() {
			adjMaxDiveTime = table.getRepetitiveDiveMaxTime('A', 0);
			expect(adjMaxDiveTime).toBe(195);
		});
	});
    
	describe("when given an repetitive dive pressure group of J and a depth of 150 feet", function(){
		it("should give an actual bottom  time of 0", function() {
			adjMaxDiveTime = table.getRepetitiveDiveMaxTime('J', 150);
			expect(adjMaxDiveTime).toBe(0);
		});
	});
    
    //Table 3 RNT
	describe("when given an repetitive dive pressure group of D and a depth of 60 feet", function(){
		it("should give a residual nitrogen time of 16", function() {
			adjMaxDiveTime = table.getResidualNitrogenTime('D', 60);
			expect(adjMaxDiveTime).toBe(16);
		});
	});
    
	describe("when given an repetitive dive pressure group of A and a depth of 0 feet", function(){
		it("should give a residual nitrogen time of 10", function() {
			adjMaxDiveTime = table.getResidualNitrogenTime('A', 0);
			expect(adjMaxDiveTime).toBe(10);
		});
	});
    
	describe("when given an repetitive dive pressure group of J and a depth of 150 feet", function(){
		it("should give an residual nitrogen time of Infinity", function() {
			adjMaxDiveTime = table.getResidualNitrogenTime('J', 150);
			expect(adjMaxDiveTime).toBe(Infinity);
		});
	});
	
});