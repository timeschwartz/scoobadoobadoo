/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

var table;
beforeEach(function(done){
    table = new NAUIDiveTable();
    table.initDiveTable(function () { 
        done(); 
    });
});

describe("NAUIDiveTable", function() {
	
	var pressureGroup;
	var adjMaxDiveTime;
	var DecompTime;
	
	//End of dive group
	describe("when given a dive of 60 feet for 40 minutes", function(){
		it("should give a pressure group of G", function() {
			pressureGroup = table.getEndOfDiveGroup(60, 40);
			expect(pressureGroup).toBe('G');
		});
	});
    
	describe("when given a dive of 140 feet for 5 minutes", function(){
		it("should give a value indicating an unsupported dive depth (undefined)", function() {
			pressureGroup = table.getEndOfDiveGroup(140, 5);            
			expect(pressureGroup).toBe(undefined);
		});
	});
	
	describe("when given a dive of 0 feet for 15 minutes", function(){
		it("should give a pressure group of B", function() {
			pressureGroup = table.getEndOfDiveGroup(0, 15);            
			expect(pressureGroup).toBe('B');
		});
	});
    
	describe("when given a dive of 80 feet for 0 minutes", function(){
		it("should give a pressure group of B", function() {
			pressureGroup = table.getEndOfDiveGroup(80, 0);            
			expect(pressureGroup).toBe('B');
		});
	});
	//Decompression Time
	describe("when given a dive of 60 feet for 40 minutes", function(){
		it("should give a Decompression time of 0", function() {
			DecompTime = table.GetStartDecompress(60, 40);
			expect(DecompTime).toBe(0);
		});
	});
	describe("when given a dive of 140 feet for 5 minutes", function(){
		it("should give a value indicating an unsupported dive depth (undefined)", function() {
			DecompTime = table.GetStartDecompress(140, 5);            
			expect(DecompTime).toBe(undefined);
		});
	});
	describe("when given a dive of 80 feet for 55 minutes", function(){
		it("should give a Decompression Time of 17", function() {
			DecompTime = table.GetStartDecompress(80, 55);            
			expect(DecompTime).toBe(17);
		});
	});
	describe("when given a dive of 110 feet for 21 minutes", function(){
		it("should give a Decompression Time of 5", function() {
			DecompTime = table.GetStartDecompress(110, 21);            
			expect(DecompTime).toBe(5);
		});
	});
		describe("when given a dive of 70 feet for 45 minutes", function(){
		it("should give a Decompression Time of 0", function() {
			DecompTime = table.GetStartDecompress(70, 45);            
			expect(DecompTime).toBe(0);
		});
	});
	
    //Surface interval time
	describe("when given an initial pressure group of H and a SIT of 120 minutes", function(){
		it("should give a pressure group of E", function() {
			pressureGroup = table.getSurfaceIntervalTimeGroup('H', 120);
			expect(pressureGroup).toBe('E');
		});
	});
	
	describe("when given an initial pressure group of A and a SIT of 10000 minutes", function(){
		it("should give a pressure group of A", function() {
			pressureGroup = table.getSurfaceIntervalTimeGroup('A', 10000);
			expect(pressureGroup).toBe('A');
		});
	});
    
	describe("when given an initial pressure group of F and a SIT of 0 minutes", function(){
		it("should give a pressure group of F", function() {
			pressureGroup = table.getSurfaceIntervalTimeGroup('F', 0);
			expect(pressureGroup).toBe('F');
		});
	});
	
    //Repetitive dive time max
	describe("when given an repetitive dive pressure group of D and a depth of 60 feet", function(){
		it("should give an adjusted maximum dive time of 31", function() {
			adjMaxDiveTime = table.getRepetitiveDiveMaxTime('D', 60);
			expect(adjMaxDiveTime).toBe(31);
		});
	});
    
	describe("when given an repetitive dive pressure group of A and a depth of 0 feet", function(){
		it("should give an adjusted maximum dive time of 123", function() {
			adjMaxDiveTime = table.getRepetitiveDiveMaxTime('A', 0);
			expect(adjMaxDiveTime).toBe(123);
		});
	});
    
	describe("when given an repetitive dive pressure group of J and a depth of 150 feet", function(){
		it("should give an adjusted maximum dive time of 0", function() {
			adjMaxDiveTime = table.getRepetitiveDiveMaxTime('J', 150);
			expect(adjMaxDiveTime).toBe(0);
		});
	});
    
    //Residual nitro time
	describe("when given an repetitive dive pressure group of D and a depth of 60 feet", function(){
		it("should give a residual nitrogen time of 24", function() {
			adjMaxDiveTime = table.getResidualNitrogenTime('D', 60);
			expect(adjMaxDiveTime).toBe(24);
		});
	});
    
	describe("when given an repetitive dive pressure group of A and a depth of 0 feet", function(){
		it("should give a residual nitrogen time of 7", function() {
			adjMaxDiveTime = table.getResidualNitrogenTime('A', 0);
			expect(adjMaxDiveTime).toBe(7);
		});
	});
    
	describe("when given an repetitive dive pressure group of J and a depth of 150 feet", function(){
		it("should give an residual nitrogen time of Infinity", function() {
			adjMaxDiveTime = table.getResidualNitrogenTime('J', 150);
			expect(adjMaxDiveTime).toBe(Infinity);
		});
	});
	
});