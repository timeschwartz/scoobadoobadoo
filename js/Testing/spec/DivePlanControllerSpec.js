/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

describe("DivePlanController", function() {

    beforeEach(module('ScoobaApp.Controllers'));
    beforeEach(module('ScoobaApp.Factories'));

    var scope;
    beforeEach(function(done){
        inject(function($rootScope, $controller, DiveTableFactory, DiveSerializerFactory) {
            scope = $rootScope.$new();
            ctrl = $controller("DivePlanController", {
                $scope: scope,
                DiveTableFactory: DiveTableFactory,
                DiveSerializerFactory: DiveSerializerFactory
            });
            scope.onInitComplete = function() 
            {
                done();
            }
        })
    });

    describe("when diving 90 feet for 20 minutes and then returning to the surface", function(){
        it("should give a resulting pressure group of F", function() {
            scope.steps = [
                {Depth: 90, Duration: 20},
                {Depth: 0, Duration: 5}
            ];
            scope.calcDiveGroup();
            expect(scope.errorText).toBe('');
            expect(scope.resultPG).toBe('F');
        });
    });

    describe("when diving 90 feet for 20 minutes and then NOT returning to the surface", function(){
        it("should set the error message and give a pressure group of Unknown", function() {
            scope.steps = [
                {Depth: 90, Duration: 20} 
            ];
            scope.calcDiveGroup();
            expect(scope.errorText).not.toBe('');
            expect(scope.resultPG).toBe('Unknown');
        });
    });
    
    describe("when diving 10 feet for 55 minutes and then returning to the surface", function(){
        it("should give a resulting pressure group of G (Dives < 40 meters are considered 40)", function() {
            scope.steps = [
                {Depth: 10, Duration: 55} 
            ];
            scope.calcDiveGroup();
            expect(scope.errorText).not.toBe('');
            expect(scope.resultPG).toBe('Unknown');
        });
    });
    
    describe("when diving 90 feet for 20 minutes, returning to the surface for 240 minutes, then diving 70 feet for 30 minutes and returning to the surface", function(){
        it("should give a resulting pressure group of F", function() {
            scope.steps = [
                {Depth: 90, Duration: 20}, 
                {Depth: 0, Duration: 240}, 
                {Depth: 70, Duration: 30}, 
                {Depth: 0,  Duration: 5}, 
            ];
            scope.calcDiveGroup();
            expect(scope.errorText).toBe('');
            expect(scope.resultPG).toBe('F');
        });
    });
    
    describe("when diving 90 feet for 20 minutes, returning to the surface for 120 minutes, then diving 70 feet for 30 minutes and returning to the surface", function(){
        it("should set the error message indicating MDT exceeded and give a pressure group of Unknown", function() {
            scope.steps = [
                {Depth: 90, Duration: 20}, 
                {Depth: 0, Duration: 120}, 
                {Depth: 70, Duration: 30}, 
                {Depth: 0,  Duration: 5}, 
            ];
            scope.calcDiveGroup();
            expect(scope.errorText).not.toBe('');
            expect(scope.resultPG).toBe('Unknown');
        });
    });
    
    describe("when diving at 140 feet for 10 minutes and then returning to the surface", function(){
        it("should set the error message indicating an unsupported dive depth and a pressure group of Unknown", function() {
            scope.steps = [
                {Depth: 140, Duration: 10},
                {Depth: 0, Duration: 5}
            ];
            scope.calcDiveGroup();
            expect(scope.errorText).not.toBe('');
            expect(scope.resultPG).toBe('Unknown');
        });
    });

    
    describe("when given a dive of 50 feet for 30 minutes and  70 feet for 10 minutes then returning to the surface", function(){
        it("should assume a 70 foot dive for 40 minutes and give a resulting pressure group of H", function() {
            scope.steps = [
                {Depth: 50, Duration: 30},
                {Depth: 70, Duration: 10},
                {Depth: 0, Duration: 5}
            ];
            scope.calcDiveGroup();
            expect(scope.errorText).toBe('');
            expect(scope.resultPG).toBe('H');
        });
    });

    
});