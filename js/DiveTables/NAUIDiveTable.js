/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

function NAUIDiveTable() {
    var _tableOne = [];
	var _tableTwo = [];
	var _tableThree = [];
	
	this.initDiveTable = function(completedCallback) {
		var sem = 0;
        $.getJSON('/js/DiveTables/JSON/NAUI_Table_1.json', [], function(data, status, jqxhr){
            _tableOne = data;
            sem += 1;
            if(sem > 2) completedCallback();
        });
		$.getJSON('/js/DiveTables/JSON/NAUI_Table_2.json', [], function(data, status, jqxhr){
			_tableTwo = data;
			sem += 1;
			if(sem > 2) completedCallback();
		});
		$.getJSON('/js/DiveTables/JSON/NAUI_Table_3.json', [], function(data, status, jqxhr){
			_tableThree = data;
			sem += 1;
			if(sem > 2) completedCallback();
		});
	}
	
	/*
		The Great Tower of ifs is dead
	*/
	this.getEndOfDiveGroup = function(Depth, DiveTime) {
    console.log(Depth)
    console.log(DiveTime)
        var i, len;
        for(i=0, len= _tableOne.length; i<len; i++){
            if(Depth <= +_tableOne[i].Depth && DiveTime <= +_tableOne[i].DiveTime){
                return _tableOne[i].StartGroup;
            }
        }
        return undefined;
	}
    this.GetStartDecompress = function(Depth, DiveTime) {
        var i, len;
        for(i=0, len= _tableOne.length; i<len; i++){
            if(Depth <= _tableOne[i].Depth && DiveTime <= _tableOne[i].DiveTime){
                return _tableOne[i].DecompTime;
            }
        }
        return undefined;
    }
	
	this.getSurfaceIntervalTimeGroup = function(StartGroup, SurfaceIntervalTime) {
		var resultGroup = StartGroup;
		var i, len;
		for(i = 0, len = _tableTwo.length; i < len; i++) {
			if(	_tableTwo[i].StartGroup == StartGroup &&
				(+_tableTwo[i].TimeMin) <= SurfaceIntervalTime &&
				(+_tableTwo[i].TimeMax) >= SurfaceIntervalTime )
			{
				resultGroup = _tableTwo[i].EndGroup;
				break;
			}
		}
		return resultGroup;
	};
	
	this.getRepetitiveDiveMaxTime = function(StartGroup, Depth) {
		var adjTime = 0;
		var i, len; 
		for(i = 0, len = _tableThree.length; i < len; i++) {
			if(	_tableThree[i].StartGroup == StartGroup &&
				_tableThree[i].Depth >= Depth )
			{
				adjTime = +_tableThree[i].AdjMaxDiveTime;
				break;
			}
		}
		return adjTime;
	};

    this.getResidualNitrogenTime = function(StartGroup, Depth) {
        var rnt = Infinity;
		var i, len;
       
		for(i = 0, len = _tableThree.length; i < len; i++) {
			if(	_tableThree[i].StartGroup == StartGroup &&
				_tableThree[i].Depth >= Depth )
			{
				rnt = +_tableThree[i].ResidualNitroTime; // Plus casts this to an integer
				break;
			}
		}
		return rnt;
    };
    
}
