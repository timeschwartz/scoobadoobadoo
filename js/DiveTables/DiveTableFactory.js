/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

angular.module('ScoobaApp.Factories')
	.factory('DiveTableFactory', function() {
		
		var service = {};
		var _diveTable = 'NAUI'; //Default dive table
	
		service.setDiveTable = function(TableName) {
			_diveTable = TableName;
		};
	
		service.createDiveTable = function () {
			return new window[_diveTable+"DiveTable"]();
		}
	
		return service;;
		
		
	});