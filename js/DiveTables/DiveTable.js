/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

function DiveTable() {

	/* Initialize the dive table (JSON Requests, etc), takes a callback as a param */
	this.initDiveTable = function(completedCallback){
		throw "Not Implemented";
	};

	/* Get the dive group after a dive at Depth for DiveTime (Minutes) */
	this.getEndOfDiveGroup = function(Depth, DiveTime) {
		throw "Not Implemented";
	};
	
	/* 	Get the dive group after a surface interval time of 
		SurfaceIntervalTime (Minutes) when starting from group StartGroup */
	this.getSurfaceIntervalTimeGroup = function(StartGroup, SurfaceIntervalTime) {
		throw "Not Implemented";
	};
	
	/* Get the MDT of a repetitive dive starting in group StartGroup and going to Depth */
	this.getRepetitiveDiveMaxTime = function(StartGroup, Depth) {
		throw "Not Implemented";
	};
}