/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

function PADIDiveTable() {
	
	var _tableOne = [];
	var _tableTwo = [];
	var _tableThree = [];

    this.initDiveTable = function(completedCallback){
        var sem = 0;
        $.getJSON('/js/DiveTables/JSON/PADI_Table_1.json',[],function(data,status,jqxhr){
            _tableOne = data.sort(function(a,b){
                var deltaDepth = a.Depth - b.Depth;
                if(deltaDepth == 0){
                    return a.Duration - b.Duration;
                }else{
                    return deltaDepth;
                }
            });
            sem+=1;
            if(sem>2)completedCallback();
        });
        $.getJSON('/js/DiveTables/JSON/PADI_Table_2.json',[],function(data,status,jqxhr){
            _tableTwo = data;
            sem+=1;
            if(sem>2)completedCallback();
        });
        $.getJSON('/js/DiveTables/JSON/PADI_Table_3.json',[],function(data,status,jqxhr){
            _tableThree = data;
            sem+=1;
            if(sem>2)completedCallback();
        });
    }
	
	this.getEndOfDiveGroup = function(Depth, DiveTime) {
    
        var i,len;
        for(i=0, len=_tableOne.length; i<len; i++){
            var e = _tableOne[i];
            if(Depth <= e.Depth && DiveTime <= e.Duration){
                if(e.Duration == Infinity) {
                    return undefined; //If we ever hit infinity, we have exceeded the dive table maximum
                }else{
                    return e.Group;
                }
            }
        } 
        return undefined;
        
	};
    
	this.GetStartDecompress = function(Depth, DiveTime) {
    
        var i,len;
        for(i=0, len=_tableOne.length; i<len; i++){
            var e = _tableOne[i];
            if(Depth <= e.Depth && DiveTime <= e.Duration){
                if(e.Duration == Infinity) {
                    return undefined; //If we ever hit infinity, we have exceeded the dive table maximum
                }else{
                    return e.DecompTime;
                }
            }
        } 
        return undefined;
        
	};
    
	this.getSurfaceIntervalTimeGroup = function(StartGroup, SurfaceIntervalTime) {
		var resultGroup = StartGroup;
        var i, len; 
        for(i=0, len = _tableTwo.length; i<len; i++){ 
            if(_tableTwo[i].StartGroup == StartGroup &&
                (+_tableTwo[i].TimeMin) <= SurfaceIntervalTime &&
                (+_tableTwo[i].TimeMax) >= SurfaceIntervalTime)
            {
                return _tableTwo[i].EndGroup;
            }
        }
        return undefined;
	};
	this.getRepetitiveDiveMaxTime = function(StartGroup, Depth) { 
        var i, len;
        for(i = 0, len = _tableThree.length; i<len; i++){
            if(_tableThree[i].StartGroup == StartGroup && Depth <= +_tableThree[i].Depth){
                return +_tableThree[i].ActualBottomTime;
            }
        }
        return 0; //No matches, the user has exceeded the max dive time
	};
    this.getResidualNitrogenTime = function(StartGroup, Depth) { 
        var i, len;
        for(i = 0, len = _tableThree.length; i<len; i++){
            if(_tableThree[i].StartGroup == StartGroup && Depth <= +_tableThree[i].Depth){
                return +_tableThree[i].ResidualNitroTime;
            }
        }
        return Infinity;
    };
    
    
}