/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

angular.module('ScoobaApp.Factories')
	.factory('DiveSerializerFactory', function() {
		
		var service = {}; 
        
        //Serializes the steps array to JSON and returns the JSON string in base64
		service.serialize = function (steps, diveName, diveLocation, selTable) {
            var toSerialize = { Name: diveName, 
                                Location: diveLocation, 
                                Steps: steps,
                                SelectedTable: selTable
                              }; 
			return btoa(JSON.stringify(toSerialize)); //Base64 for easy transfer via url, etc.
		}
        
        //Deserializes a JSON string in base64.
        //This function returns false on failure and a deserialized object otherwise.
        service.deserialize = function(input) {
            try{
             
                var decoded = atob(input); //Decode base64
                var deserialized = JSON.parse(decoded); //Deserialize JSON 
                
                //Ensure all needed properties exist
                if( deserialized.Name === undefined     ||
                    deserialized.Location === undefined ||
                    deserialized.Steps === undefined    ||
                    deserialized.SelectedTable === undefined    )
                {
                    return false;
                }
                
                return deserialized;
                
            }catch(e){
                return false;
            }
        }
	
		return service;
		
		
	});