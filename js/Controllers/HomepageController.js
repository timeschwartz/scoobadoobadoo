/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

var HomepageController = function ($scope, DiveTableFactory)
{
	$scope.group = 'H';
	$scope.divetime = 120;
	$scope.divetable = 'NAUI';
	$scope.depth = 40;
	
	//Create the dive table
	DiveTableFactory.setDiveTable($scope.divetable);
	var _diveTable = DiveTableFactory.createDiveTable();
	
	//Initialize the dive table, returns a callback because of networking
	_diveTable.initDiveTable(function(){
		$scope.group = _diveTable.getSurfaceIntervalTimeGroup($scope.group, $scope.divetime);
		$scope.$apply(); //This lambda occurs outside of the angular framework, so we must manually update bindings
	});
}

angular.module('ScoobaApp.Controllers')
	.controller('HomepageController', ['$scope', 'DiveTableFactory', HomepageController]);
