/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

var NavigationController = function ($scope, $location)
{
	$scope.isActive = function(route) {
        return route === $location.path();
    }
}

var module = angular.module('ScoobaApp.Controllers');
module.controller('NavigationController', ['$scope', '$location', NavigationController]);
module.config(function($routeProvider) {
	$routeProvider

		.when('/', {
			templateUrl : 'template/home.php',
			controller  : 'HomepageController'
		})

		.when('/signup', {
			templateUrl : 'template/signup.php',
			controller  : 'SignupController'
		})

		.when('/login', {
			templateUrl : 'template/login.php',
			controller  : 'LoginController'
		})

		.when('/diveplan/:d', {
			templateUrl : 'template/diveplan.php',
			controller  : 'DivePlanController'
		})

		.when('/diveplan', {
			templateUrl : 'template/diveplan.php',
			controller  : 'DivePlanController'
		});
});