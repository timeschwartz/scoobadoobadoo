/*
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
*/

var DivePlanController = function ($scope, $routeParams, DiveTableFactory, DiveSerializerFactory)
{

	//Create the dive table
    $scope.selectedTable = 'NAUI';
	DiveTableFactory.setDiveTable($scope.selectedTable);
	$scope.diveTable = DiveTableFactory.createDiveTable();
    $scope.tableLoading = true;
    
    $scope.onInitComplete = function() {
        /* If you need to call something after all init is done (Used in Jasmine) */ 
    };
    
    //Will be called after the dive table loads
    $scope.finializeInit = function() {
        /* User inputs */
        $scope.steps = [];
        $scope.diveName = "";
        $scope.diveLocation = "";
        $scope.resultPG = "A";
        $scope.totalDiveDuration = 0;
        $scope.serializedDive = '';
        $scope.totalNitroTime = 0;
        $scope.$watch('selectedTable', function(newValue, oldValue){   
            if (newValue == oldValue){
                return; 
            }
            $scope.tableLoading = true;
            DiveTableFactory.setDiveTable($scope.selectedTable);
            $scope.diveTable = DiveTableFactory.createDiveTable();             
            //Initialize the dive table, returns a callback because of ajax
            $scope.diveTable.initDiveTable(function(){
                $scope.tableLoading = false;
                $scope.updateAll();
                $scope.$apply();
            });
        });
        
        $scope.decompStops = [];
        /* Control Vars */
        $scope.errorText = "";
        
        /* Dive Plan Functions */
        $scope.calcDuration = function() {
            $scope.totalUnderwaterDuration = 0;
            $scope.totalDiveDuration = 0;
            for(var i = 0, len = $scope.steps.length; i < len; i++) {
                if($scope.steps[i].Depth != 0) $scope.totalUnderwaterDuration += +$scope.steps[i].Duration;
                $scope.totalDiveDuration += +$scope.steps[i].Duration;
            }
        };
        
        $scope.addstep = function() {
            $scope.steps.push({
                Depth: 0,
                Duration: 30
            });
            $scope.updateAll();
        };
        
        $scope.updateAll = function() {
            //Fix bad steps
            for(var i = 0, len = $scope.steps.length; i < len; i++){ 
                $scope.steps[i].Duration = +($scope.steps[i].Duration+'').split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
                $scope.steps[i].Depth = +($scope.steps[i].Depth+'').split('').filter(function (s) { return (!isNaN(s) && s != ' '); }).join('');
            } 
            $scope.d3data();
            $scope.calcDuration();
            $scope.calcDiveGroup();

        };
        
        $scope.calcDiveGroup = function() {
            var repetitive = false;
            var group = 'A';
                        
            //Make a clone of the steps
            var steps = $scope.steps.slice(0);
            
            //Clear decomps
            $scope.decompStops = []
            
            //Append a surface step to the beginning (All dives must start from the surface)
            steps.unshift({
                Depth: 0,
                Duration: 0
            });
            
            //Squarify dive (Combine surface times next to eachother and dives next to eachother)
            var squaresteps = []
            var isSurface = true;
            var durationAccumulation = 0;
            var depthMaximum = 0;
            for(var i = 0, len = steps.length; i < len; i++){
                var step = steps[i];
                if(step.Depth == 0){
                    if(!isSurface){ 
                        squaresteps.push({Depth: depthMaximum, Duration: durationAccumulation});
                        durationAccumulation = 0;
                        depthMaximum = 0;
                        isSurface = true;
                    }
                    durationAccumulation += +step.Duration;
                }else{
                    if(isSurface){
                        squaresteps.push({Depth: 0, Duration: durationAccumulation});
                        durationAccumulation = 0;
                        isSurface = false; 
                    }
                    durationAccumulation += +step.Duration;
                    depthMaximum = Math.max(depthMaximum, +step.Depth);
                }
            }
            //console.log(squaresteps);
            for(var i = 1, len = squaresteps.length; i < len; i++){
            
                var step = squaresteps[i]; 
                if(step.Depth == 0){
                
                    //Returned to surface, all further dives are repetitive
                    repetitive = true;
                    
                    //Get SIT group
                    group = $scope.diveTable.getSurfaceIntervalTimeGroup(group, step.Duration)
                                        
                }else{
                
                    if(repetitive){

                        //Repetitive dive, calculate the MDT and RNT
                        mdt = $scope.diveTable.getRepetitiveDiveMaxTime(group, step.Depth) 
                        rnt = $scope.diveTable.getResidualNitrogenTime(group, step.Depth)  
                        if( step.Duration > mdt ){
                            $scope.errorText = "Maximum dive time exceeded ("+mdt+" minutes)";
                            $scope.resultPG = 'Unknown'; 
                            $scope.totalNitroTime = 'Unknown'; 
                            return
                        }
                         
                        $scope.totalNitroTime = step.Duration + rnt;
                        group = $scope.diveTable.getEndOfDiveGroup(step.Depth, step.Duration)
  
                    }else{
                    
                        //First dive
                        group = $scope.diveTable.getEndOfDiveGroup(step.Depth, step.Duration);
                        
                        if(group == undefined){
                            $scope.errorText = "Unsupported Depth.  Dive too deep.";
                            $scope.resultPG = 'Unknown'; 
                            $scope.totalNitroTime = 'Unknown'; 
                            return
                        }
                        
                    }
                        
                    //Check if the diver will need a decompression stop
                    decomp = $scope.diveTable.GetStartDecompress(step.Depth, step.Duration);
                    console.log(decomp)
                    if(decomp > 0){
                        $scope.decompStops.push({
                            Duration: decomp,
                            StepIndex: i
                        });
                    }
                    
                }
                
            }
              
            if($scope.steps.length > 0 && $scope.steps[$scope.steps.length-1].Depth != 0){
                $scope.errorText = "Please add a final surface step.";
                $scope.resultPG = 'Unknown';
            }else{
                $scope.errorText = "";
                $scope.resultPG = group;
            }
            
            
        }
        
        $scope.discardDive = function() {
            $scope.steps = [];
            $scope.diveName = "";
            $scope.diveLocation = "";
            $scope.errorText = "";
            $scope.calcDuration();
            $scope.calcDiveGroup();
            $scope.serializedDive = '';
        };
        
        $scope.saveDive = function() {
        
            if($scope.errorText === ""){ 
                $scope.serializedDive = serialized = DiveSerializerFactory.serialize($scope.steps, $scope.diveName, $scope.diveLocation, $scope.selectedTable); 
            }else{
                alert("Please fix any errors before saving");
            }            
            
        };
        
        $scope.removestep = function(step) {
            var idx = $scope.steps.indexOf(step);
            if(idx > -1) {
                $scope.steps.splice(idx,1);
            }
            $scope.updateAll();
        };
        
        $scope.d3margin = {top: 30, right: 20, bottom: 30, left: 50};
        $scope.d3width  = 800 - $scope.d3margin.left - $scope.d3margin.right;
        $scope.d3height = 400 - $scope.d3margin.top - $scope.d3margin.bottom;

        $scope.d3data = function() {

            $('#d3container').html("");
            var data_unformatted = $scope.steps;
            var data = [{
                Depth: 0,
                Duration: 0
            }];
            var timeAccum = 1;
            for( var i = 0, len = data_unformatted.length; i < len; i ++){
                data.push({
                    Depth: data_unformatted[i].Depth,
                    Duration: timeAccum + 1
                });
                timeAccum += +data_unformatted[i].Duration
                data.push({
                    Depth: data_unformatted[i].Depth,
                    Duration: timeAccum - 1
                });
            }
            
            
            var xAccumulator = 0;
            
            // Set the ranges
            var x = d3.scale.linear().range([0, $scope.d3width]);
            var y = d3.scale.linear().range([0, $scope.d3height]);

            // Define the axes
            var xAxis = d3.svg.axis().scale(x)
                .orient("bottom").ticks(5);

            var yAxis = d3.svg.axis().scale(y)
                .orient("left").ticks(5);
                
            //Define line
            var valueline = d3.svg.line()
                    .x(function(d) { return x(d.Duration); })
                    .y(function(d) { return y(d.Depth); });
                    
            // Scale the range of the data
            x.domain(d3.extent(data, function(d) { return +d.Duration; }));
            y.domain([0, d3.max(data, function(d) { return +d.Depth; })]);
            // Adds the svg canvas
            var svg = d3.select("#d3container")
                .append("svg")
                    .attr("width", $scope.d3width + $scope.d3margin.left + $scope.d3margin.right)
                    .attr("height", $scope.d3height + $scope.d3margin.top + $scope.d3margin.bottom)
                .append("g")
                    .attr("transform", 
                          "translate(" + $scope.d3margin.left + "," + $scope.d3margin.top + ")");

            // Add the valueline path.
            svg.append("path")
                .attr("class", "line")
                .attr("d", valueline(data));

            // Add the X Axis
            svg.append("g")
                .attr("class", "x axis")
                .call(xAxis);
                
            svg.append("text")
                .attr("class", "x label")
                .attr("text-anchor", "end")
                .attr("x", $scope.d3width)
                .attr("y", -3)
                .text("Minutes");

            // Add the Y Axis
            svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);
                
            svg.append("text")
                .attr("class", "y label")
                .attr("text-anchor", "end")
                .attr("y", $scope.d3height + 5)
                .attr("x", -3)
                .attr("dy", ".75em") 
                .text("Feet");
                
        };
        $scope.d3data();
        
        //Check if the user is importing a dive
        if($routeParams.d !== undefined){
            
            //Import the dive
            var deserialized = DiveSerializerFactory.deserialize($routeParams.d);
            
            //Set scope vars
            $scope.diveName = deserialized.Name;
            $scope.diveLocation = deserialized.Location;
            $scope.steps = deserialized.Steps;
            $scope.selectedTable = deserialized.SelectedTable;
            
            $scope.tableLoading = true;
            DiveTableFactory.setDiveTable($scope.selectedTable);
            $scope.diveTable = DiveTableFactory.createDiveTable();             
            //Initialize the dive table, returns a callback because of ajax
            $scope.diveTable.initDiveTable(function(){
                $scope.tableLoading = false;
                $scope.updateAll();
                $scope.$apply();
            });
            
        }
        
    }
        
	//Initialize the dive table, returns a callback because of ajax
	$scope.diveTable.initDiveTable(function(){
        $scope.tableLoading = false;
        $scope.finializeInit();
        $scope.$apply();
        $scope.onInitComplete();
    });

    
}

//Ensure numeric inputs are truly numeric
angular.module('ScoobaApp.Controllers')
    .directive('stringToNumber', function() {
      return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
          ngModel.$parsers.push(function(value) {
            return '' + value;
          });
          ngModel.$formatters.push(function(value) {
            return parseFloat(value, 10);
          });
        }
      };
});

angular.module('ScoobaApp.Controllers')
	.controller('DivePlanController', ['$scope', '$routeParams', 'DiveTableFactory', 'DiveSerializerFactory', DivePlanController]);

