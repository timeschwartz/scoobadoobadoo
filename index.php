<!--
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
-->

<!DOCTYPE html>
<html lang="en" ng-app="ScoobaApp">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Scoobadoobadoo</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body ng-controller="NavigationController">

	<nav class="navbar navbar-inverse navbar-default" >
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Scoobadoobadoo</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li ng-class="{active:isActive('/')}"><a href="#/">Home</a></li>
                    <li ng-class="{active:isActive('/diveplan')}"><a href="#/diveplan">Dive Planner</a></li>
				</ul>
			</div>
		</div>
	</nav>

    <div id="MainContent" class="container" ng-view>

      
    </div><!-- /.container -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.13/angular-route.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
	<script src="js/ScoobaApp.js"></script>
	<script src="js/ScoobaApp.Factories.js"></script>
	<script src="js/DiveSerializerFactory.js"></script>
	<script src="js/DiveTables/DiveTable.js"></script>
	<script src="js/DiveTables/NAUIDiveTable.js"></script>
	<script src="js/DiveTables/PADIDiveTable.js"></script>
	<script src="js/DiveTables/DiveTableFactory.js"></script>
	<script src="js/Controllers/ScoobaApp.Controllers.js"></script>
	<script src="js/Controllers/NavigationController.js"></script>
	<script src="js/Controllers/HomepageController.js"></script>
	<script src="js/Controllers/DivePlanController.js"></script>
	<?php include ('template/disclaimer.php');?>
  </body>
</html>
