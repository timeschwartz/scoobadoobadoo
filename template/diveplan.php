<!--
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
-->

<div class="row fill-height">
    <div class="col-md-3 sidebar">
        <div class="dive-input"><input type="text" placeholder="Dive Name" ng-model="diveName" maxlength="50"/></div>
        <div class="dive-input"><input type="text" placeholder="Location" ng-model="diveLocation" maxlength="100"/></div> 
        <div class="dive-input">
            <select ng-model="selectedTable">
                <option value="NAUI" selected="selected">NAUI</option>
                <option value="PADI">PADI</option>
            </select>
        </div>
        <div class="dive-input" ng-show="tableLoading">
            <img src="/img/loading.gif" style="width:35px; height=35px;" /> Table Loading . . .
        </div>
        <div class="section-header"><span class="header-text">Steps</span></div>
        <div>
            <input class="btn btn-danger" type="button" value="X" style="visibility:hidden" />
            <input class="divestep-text" type="text" value="0"  disabled="disabled"/> Ft.
            <input class="divestep-text" type="text" value="0" disabled="disabled"/> Min.   
        </div>
        <div id="divesteps">
            <div class="divestep" ng-repeat="step in steps track by $index">
                <input class="btn btn-danger" type="button" value="X" ng-click="removestep(step)" />
                <input class="divestep-text" type="number" string-to-number min="0" max="140" ng-model="step.Depth" value="{{step.Depth}}" ng-change="updateAll()"/> Ft.
                <input class="divestep-text" type="number" min="0" max="1439" ng-model="step.Duration" value="{{step.Duration}}" ng-change="updateAll()" /> Min.                
            </div>
        </div>
        <div class="dive-input">
            <input class="btn btn-success" type="button" ng-click="addstep()" value="Add Step" input/>
        </div>
    </div>
    <div class="col-md-9 diveplan">
        <div class="row"><div id="d3container"></div></div>
        <div class="row" style="margin-top: 5px">
            <div>
                <span class="stat-title">Resulting Pressure Group: </span>
                <span class="stat-title" ng-class="">{{resultPG}}</span>
            </div>
            <div>
                <span class="stat-title">Total Time: </span>
                <span class="stat-title">{{totalDiveDuration}}</span>
            </div>
            <div>
                <span class="stat-title">Total Time Below Surface: </span>
                <span class="stat-title">{{totalUnderwaterDuration}}</span>
            </div>
            <div>
                <span class="stat-title">Total Nitrogen Time: </span>
                <span class="stat-title">{{totalNitroTime}}</span>
            </div>
        </div>
        <div class="row" ng-hide="steps.length == 0" style="margin-top: 5px">
            <div class="bg-warning" ng-hide="decompStops.length == 0">
                <div ng-repeat="stop in decompStops">
                    Decompression stop for {{stop.Duration}} minutes at 15 feet required after step {{stop.StepIndex}}
                </div>
            </div>
            <div class="error-text bg-danger" ng-hide="errorText.length == 0">{{errorText}}</div>
            <div class="pull-right">
                <input class="btn btn-success" type="button" value="Export Dive" ng-click="saveDive()" />
                <input class="btn btn-danger" type="button" value="Start Over" ng-click="discardDive()" />
            </div>
        </div>
        <div class="row" ng-hide="serializedDive == ''">
            <h3>Save this dive</h3>
            <span>Use this URL to share or revisit this dive: <input size="50" type="text" onclick="this.select()" value="localhost/#/diveplan/{{serializedDive}}" /></span><br /> 
        </div>
    </div>
</div>