<!--
      WARNING! While we at scoobadoobadoo strive for perfection, 
	  we cannot guarantee it. This product is just a prototype and 
	  should never ever be use to plan real dives. The use of this product 
	  may and will most likely result in serious injury and/or a horribly 
	  painful death. We are not liable for any pain and suffering caused 
	  by the usage of this product.
-->

<div class="main-content">
	<h1>Scoobadoobadoo</h1>
	<p class="lead">The best dive planner ever.</p>
	<p>Description: 
Scoobadoobadoo is a user friendly dive planner that is designed to be used by just about every level of diver ranging from beginner to master. It is used to easily and safely plan any dive that ranges from 0 ft to 140 ft! Scoobadoobadoo allows the user freedom in selecting depth and range and it will warn them if the depth and time that they selected is not safe. Scoobadoobadoo also has a dive graph that changes according to the time and depths the user selects. It enables them to see for themselves what their dive is going to look like based on depth and time. 
So who are the masterminds behind Scoobadoobadoo? They are Alex Plummer, Tim Schwartz, and Chang Vang. Together, these three has created a dive planner unlike any other you will ever see.  
</p>
<p>
Faq: <br/>
Why should I use it? <br/>
You should use it if you are tired of scratching your head raw when you look up a dive table to plan your dive. Scoobadoobadoo enables you to plan your own dive with a few clicks of the mouse.
<br/><br/>
Who should use it?<br/>
Scoobadoobadoo is design for every diver, even those who have just started. <br/>
<br/>
What is the max depth allowed in Scoobadoobadoo? <br/>
The max depth will depend on the dive table you select.
<br/><br/>
What is the max time allowed?<br/>
The maximum time allowed will vary depending on the depth of the dive.
</p><br/>
How to use:<br/> 
Input the name of the dive in the Dive Name box, and the location in the location box. Next, select the dive table you wish to use.  Then click on the add step button and select how deep you want to dive and for how long. You will need to come back up to the surface so add another step with the depth being 0 ft. Also indicate how long you plan on staying on the surface. Whenever your time underwater exceeds the maximum allowed you will be given a warning to fix the error. You will also get this error if you don’t stay on the surface long enough. Additionally, you will be warned if you need to take a decompression stop at any point.  Lastly, you can export the dive to share with friends or to review later by pressing the Export Dive button and saving the generated URL.  Happy Diving.
</p>
</div>